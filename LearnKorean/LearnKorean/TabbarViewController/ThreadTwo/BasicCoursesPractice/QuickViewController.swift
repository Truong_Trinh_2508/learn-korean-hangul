//
//  QuickViewController.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 29/11/2021.
//

import UIKit

class QuickViewController: UIViewController {
    
    var listSpokenCourses:[DailyPhrasesModel] = [DailyPhrasesModel]()
    var listDaiyPhrases:[DailyPhrasesModel] = [DailyPhrasesModel]()
    var listConsonants:[LettersModel] = [LettersModel]()
    var listConsonants1:[LettersModel] = [LettersModel]()
    var contentCourses:String!
    var indexpro:Int!
    var listQues:[LettersModel] = [LettersModel]()
    var listWrongAns:[LettersModel] = [LettersModel]()
    var arrAns:[LettersModel] = [LettersModel]()
    var listQues1:[DailyPhrasesModel] = [DailyPhrasesModel]()
    var listWrongAns1:[DailyPhrasesModel] = [DailyPhrasesModel]()
    var arrAns1:[DailyPhrasesModel] = [DailyPhrasesModel]()
    var Answer:String = ""
    var check:Bool = true
    
    @IBOutlet weak var btnNextAnswer: UIButton!
    @IBOutlet weak var btnAnswerD: UIButton!
    @IBOutlet weak var btnAnswerC: UIButton!
    @IBOutlet weak var btnAnswerB: UIButton!
    @IBOutlet weak var btnAnswerA: UIButton!
    @IBOutlet weak var lbAnswerD: UILabel!
    @IBOutlet weak var lbAnswerC: UILabel!
    @IBOutlet weak var lbAnswerB: UILabel!
    @IBOutlet weak var lbAnswerA: UILabel!
    @IBOutlet weak var imgAnswerD: UIImageView!
    @IBOutlet weak var imgAnswerC: UIImageView!
    @IBOutlet weak var imgAnswerB: UIImageView!
    @IBOutlet weak var imgAnswerA: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getDataSpokenCourses(){_,_ in}
        listConsonants = SQLiteService.shared.getConsonantsCourses(type: self.contentCourses)
        for i in listConsonants{
            if i.letterExamplesOriginal != ""{
                listConsonants1.append(i)
            }
        }
        if indexpro == 1{
            listQues1 = listSpokenCourses
            listWrongAns1 = listSpokenCourses
            btnNextAnswer.isUserInteractionEnabled = false
            let index = Int.random(in: 0..<listQues1.count)
            let correctAns = listQues1[index]
            Answer = correctAns.DetailOriginalText
            arrAns1.append(correctAns)
            var i = 3
            while i > 0{
                var index1 = Int.random(in: 0..<listWrongAns1.count)
                while listWrongAns1[index1].id == correctAns.id{
                    index1 = Int.random(in: 0..<listWrongAns1.count)
                }
                let wrongAns1 = listWrongAns1[index1]
                arrAns1.append(wrongAns1)
                listWrongAns1.remove(at: index1)
                i -= 1
            }
            arrAns1.shuffle()
            lbAnswerA.text = arrAns1[0].DetailOriginalText
            
            lbAnswerB.text = arrAns1[1].DetailOriginalText
            
            lbAnswerC.text = arrAns1[2].DetailOriginalText
            
            lbAnswerD.text = arrAns1[3].DetailOriginalText
            
            listWrongAns1 = listSpokenCourses
            listQues1.remove(at: index)
        }
        else {
            listQues = listConsonants1
            listWrongAns = listConsonants1
            btnNextAnswer.isUserInteractionEnabled = false
            let index = Int.random(in: 0..<listQues.count)
            let correctAns = listQues[index]
            Answer = correctAns.letter
            arrAns.append(correctAns)
            var i = 3
            while i > 0{
                var index1 = Int.random(in: 0..<listWrongAns.count)
                while listWrongAns[index1].id == correctAns.id{
                    index1 = Int.random(in: 0..<listWrongAns.count)
                }
                let wrongAns1 = listWrongAns[index1]
                arrAns.append(wrongAns1)
                listWrongAns.remove(at: index1)
                i -= 1
            }
            arrAns.shuffle()
            lbAnswerA.text = arrAns[0].letter
            
            lbAnswerB.text = arrAns[1].letter
            
            lbAnswerC.text = arrAns[2].letter
            
            lbAnswerD.text = arrAns[3].letter
            
            listWrongAns = listConsonants1
            listQues.remove(at: index)}
    }
    func getDataSpokenCourses(andCompletion completion:@escaping(_ moviesResponse: [DailyPhrasesModel], _ error: Error?) -> ()){
        SQLiteService.shared.DailyPhrases(){ repond,error in
            if let repond = repond{
                self.listDaiyPhrases.removeAll()
                self.listDaiyPhrases = repond
            }
            completion(self.listDaiyPhrases, error)
        }
        for item in listDaiyPhrases{
            if item.UnitName == contentCourses {
                listSpokenCourses.append(item)
            }
        }
        
    }
    
    @IBAction func AnswerSpeak(_ sender: Any) {
        TextToSpeech.shared.readText(text:Answer)
    }
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnNextAnswer(_ sender: Any) {
        if listQues.count > 0 && indexpro == 0 {
            //            lblScore.text = "\(correct)"
            check = true
            btnAnswerA.isUserInteractionEnabled = true
            btnAnswerB.isUserInteractionEnabled = true
            btnAnswerC.isUserInteractionEnabled = true
            btnAnswerD.isUserInteractionEnabled = true
            btnNextAnswer.isUserInteractionEnabled = false
            
            imgAnswerA.image = UIImage(named: "Nomal")
            imgAnswerB.image = UIImage(named: "Nomal")
            imgAnswerC.image = UIImage(named: "Nomal")
            imgAnswerD.image = UIImage(named: "Nomal")
            
            let index = Int.random(in: 0..<listQues.count)
            let correctAns = listQues[index]
            Answer = correctAns.letter
            arrAns.removeAll()
            arrAns.append(correctAns)
            var i = 3
            while i > 0{
                var index1 = Int.random(in: 0..<listWrongAns.count)
                while listWrongAns[index1].id == correctAns.id{
                    index1 = Int.random(in: 0..<listWrongAns.count)
                }
                let wrongAns1 = listWrongAns[index1]
                arrAns.append(wrongAns1)
                listWrongAns.remove(at: index1)
                i -= 1
            }
            arrAns.shuffle()
            
            lbAnswerA.text = arrAns[0].letter
            
            lbAnswerB.text = arrAns[1].letter
            
            lbAnswerC.text = arrAns[2].letter
            
            lbAnswerD.text = arrAns[3].letter
            
            listWrongAns = listConsonants1
            listQues.remove(at: index)
        }
        if listQues1.count > 0 && indexpro == 1 {
            //            lblScore.text = "\(correct)"
            check = true
            btnAnswerA.isUserInteractionEnabled = true
            btnAnswerB.isUserInteractionEnabled = true
            btnAnswerC.isUserInteractionEnabled = true
            btnAnswerD.isUserInteractionEnabled = true
            btnNextAnswer.isUserInteractionEnabled = false
            
            imgAnswerA.image = UIImage(named: "Nomal")
            imgAnswerB.image = UIImage(named: "Nomal")
            imgAnswerC.image = UIImage(named: "Nomal")
            imgAnswerD.image = UIImage(named: "Nomal")
            
            let index = Int.random(in: 0..<listQues1.count)
            let correctAns = listQues1[index]
            Answer = correctAns.DetailOriginalText
            arrAns1.removeAll()
            arrAns1.append(correctAns)
            var i = 3
            while i > 0{
                var index1 = Int.random(in: 0..<listWrongAns1.count)
                while listWrongAns1[index1].id == correctAns.id{
                    index1 = Int.random(in: 0..<listWrongAns1.count)
                }
                let wrongAns1 = listWrongAns1[index1]
                arrAns1.append(wrongAns1)
                listWrongAns1.remove(at: index1)
                i -= 1
            }
            arrAns1.shuffle()
            
            lbAnswerA.text = arrAns1[0].DetailOriginalText
            
            lbAnswerB.text = arrAns1[1].DetailOriginalText
            
            lbAnswerC.text = arrAns1[2].DetailOriginalText
            
            lbAnswerD.text = arrAns1[3].DetailOriginalText
            
            listWrongAns1 = listSpokenCourses
            listQues1.remove(at: index)
        }
    }
    
    @IBAction func btnAcAnswerA(_ sender: Any) {
        if check == true{
            check = false
            btnNextAnswer.isUserInteractionEnabled = true
            btnAnswerA.isUserInteractionEnabled = false
            btnAnswerB.isUserInteractionEnabled = false
            btnAnswerC.isUserInteractionEnabled = false
            btnAnswerD.isUserInteractionEnabled = false
            if lbAnswerA.text == Answer{
                //                correct += 1
                imgAnswerA.image = UIImage(named: "True")
            }
            else{
                //                fail += 1
                imgAnswerA.image = UIImage(named: "False")
            }
        }
    }
    @IBAction func btnAcAnswerB(_ sender: Any) {
        if check == true{
            check = false
            btnNextAnswer.isUserInteractionEnabled = true
            btnAnswerA.isUserInteractionEnabled = false
            btnAnswerB.isUserInteractionEnabled = false
            btnAnswerC.isUserInteractionEnabled = false
            btnAnswerD.isUserInteractionEnabled = false
            if lbAnswerB.text == Answer{
                //                correct += 1
                imgAnswerB.image = UIImage(named: "True")
            }
            else{
                //                fail += 1
                imgAnswerB.image = UIImage(named: "False")
            }
        }
    }
    @IBAction func btnAcAnswerC(_ sender: Any) {
        if check == true{
            check = false
            btnNextAnswer.isUserInteractionEnabled = true
            btnAnswerA.isUserInteractionEnabled = false
            btnAnswerB.isUserInteractionEnabled = false
            btnAnswerC.isUserInteractionEnabled = false
            btnAnswerD.isUserInteractionEnabled = false
            if lbAnswerC.text == Answer{
                //                correct += 1
                imgAnswerC.image = UIImage(named: "True")
            }
            else{
                //                fail += 1
                imgAnswerC.image = UIImage(named: "False")
            }
        }
    }
    @IBAction func btnAcAnswerD(_ sender: Any) {
        if check == true{
            check = false
            btnNextAnswer.isUserInteractionEnabled = true
            btnAnswerA.isUserInteractionEnabled = false
            btnAnswerB.isUserInteractionEnabled = false
            btnAnswerC.isUserInteractionEnabled = false
            btnAnswerD.isUserInteractionEnabled = false
            if lbAnswerD.text == Answer{
                //                correct += 1
                imgAnswerD.image = UIImage(named: "True")
            }
            else{
                //                fail += 1
                imgAnswerD.image = UIImage(named: "False")
            }
        }
    }
}
