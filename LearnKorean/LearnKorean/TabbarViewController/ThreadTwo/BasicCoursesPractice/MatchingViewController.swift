//
//  MatchingViewController.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 29/11/2021.
//

import UIKit

class MatchingViewController: UIViewController {
    var listSpokenCourses:[DailyPhrasesModel] = [DailyPhrasesModel]()
    var listDaiyPhrases:[DailyPhrasesModel] = [DailyPhrasesModel]()
    var listConsonants:[LettersModel] = [LettersModel]()
    var listConsonants1:[LettersModel] = [LettersModel]()
    var contentCourses:String!
    var indexpro:Int!
    var arrAns:[[String]] = [[]]
    var arrAns1:[[String]] = [[]]
    var listMatching:[String] = []
    var listMatching1:[String] = []
    var indexSelect : Int = 0
    var isChoose:Bool = false
    var indexSelected:[Int] = []
    var arrSelected:[String] = []
    var score:Int = 0
    @IBOutlet weak var MatchingCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        MatchingCollectionView.register(UINib(nibName: MatchingCell.className, bundle: nil), forCellWithReuseIdentifier: MatchingCell.className)
        
        self.getDataSpokenCourses(){_,_ in}
        listConsonants = SQLiteService.shared.getConsonantsCourses(type: self.contentCourses)
        for i in listConsonants{
            if i.letterExamplesOriginal != ""{
                listConsonants1.append(i)
            }
        }
        if indexpro == 0{
            listConsonants1.shuffle()
            for item in listConsonants1[0...5]{
                listMatching.append(item.letterExamplesOriginal)
                listMatching.append(item.letterExamplesOriginalVoice)
                arrAns.append([item.letterExamplesOriginal, item.letterExamplesOriginalVoice])
                print(arrAns)
            }
            listMatching.shuffle()
        }
        else {
            listSpokenCourses.shuffle()
            for item in listSpokenCourses[0...5]{
                listMatching.append(item.DetailOriginalText)
                listMatching.append(item.DetailTranslateText)
                arrAns.append([item.DetailOriginalText, item.DetailTranslateText])
                print(arrAns)
            }
            listMatching.shuffle()
        }
    }
    func getDataSpokenCourses(andCompletion completion:@escaping(_ moviesResponse: [DailyPhrasesModel], _ error: Error?) -> ()){
        SQLiteService.shared.DailyPhrases(){ repond,error in
            if let repond = repond{
                self.listDaiyPhrases.removeAll()
                self.listDaiyPhrases = repond
            }
            completion(self.listDaiyPhrases, error)
        }
        for item in listDaiyPhrases{
            if item.UnitName == contentCourses {
                listSpokenCourses.append(item)
            }
        }
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension MatchingViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return listMatching.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MatchingCell", for: indexPath) as! MatchingCell
        cell.lbWords.text = listMatching[indexPath.item]
        if cell.lbWords.text == " "{
            cell.isUserInteractionEnabled = false
            cell.imgMatching.image = UIImage(named: "")
        }
        else{
            cell.isUserInteractionEnabled = true
            
            if indexSelected.contains(indexPath.row) && indexSelected.count <= 2 {
                cell.imgMatching.image = UIImage(named: "Choose")
                cell.isUserInteractionEnabled = false
            } else {
                cell.imgMatching.image = UIImage(named: "UnChoose")
                cell.isUserInteractionEnabled = true
            }
        }
        return cell

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        indexSelect = indexPath.item
        indexSelected.append(indexSelect)
        MatchingCollectionView.reloadData()
        if indexSelected.count == 2 {
            for item in indexSelected{
                arrSelected.append(listMatching[item])
                print(arrSelected)
            }
            if arrSelected.count == 2{
                if arrAns.contains(arrSelected){
                    print(listMatching)
                    listMatching[indexSelected[0]] = " "
                    listMatching[indexSelected[1]] = " "
                    isChoose = true
                    print(listMatching)
                    arrSelected.removeAll()
                    
                }
                else{
                    isChoose = false
                    print("sai me roi")
                    arrSelected.removeAll()
                }
            }
            Timer.scheduledTimer(withTimeInterval: 0.3, repeats: true) { timer in
                print("timer fired!")
                
                self.MatchingCollectionView.reloadData()
                self.indexSelected.removeAll()
                timer.invalidate()
            }
        }
    }
}
extension MatchingViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 10
        }else{
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 10
        }else{
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: self.MatchingCollectionView.frame.width/3.1, height: self.MatchingCollectionView.frame.height/4.5)
        }else{
            return CGSize(width: self.MatchingCollectionView.frame.width/3.1, height: self.MatchingCollectionView.frame.height/4.3)
        }
    }
}
