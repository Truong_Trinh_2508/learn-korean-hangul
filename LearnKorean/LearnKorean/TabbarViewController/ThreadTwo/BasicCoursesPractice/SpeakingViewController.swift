//
//  SpeakingViewController.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 29/11/2021.
//

import UIKit

class SpeakingViewController: UIViewController {
    var listSpokenCourses:[DailyPhrasesModel] = [DailyPhrasesModel]()
    var listDaiyPhrases:[DailyPhrasesModel] = [DailyPhrasesModel]()
    var listConsonants:[LettersModel] = [LettersModel]()
    var listConsonants1:[LettersModel] = [LettersModel]()
    var contentCourses:String!
    var index:Int!
    var indexSelect : Int = -1
    @IBOutlet weak var SpeakingCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getDataSpokenCourses(){_,_ in}
        SpeakingCollectionView.register(UINib(nibName: SpeakingCell.className, bundle: nil), forCellWithReuseIdentifier: SpeakingCell.className)
        listConsonants = SQLiteService.shared.getConsonantsCourses(type: self.contentCourses)
        for i in listConsonants{
            if i.letterExamplesOriginal != ""{
                listConsonants1.append(i)
            }
        }
    }
    func getDataSpokenCourses(andCompletion completion:@escaping(_ moviesResponse: [DailyPhrasesModel], _ error: Error?) -> ()){
        SQLiteService.shared.DailyPhrases(){ repond,error in
            if let repond = repond{
                self.listDaiyPhrases = repond
                self.SpeakingCollectionView.reloadData()
            }
        }
        for item in listDaiyPhrases{
            if item.UnitName == contentCourses {
                listSpokenCourses.append(item)
            }
        }
    }
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
   
}
extension SpeakingViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if index == 0 {
            return listConsonants1.count
        }
        return listSpokenCourses.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if index == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SpeakingCell", for: indexPath) as! SpeakingCell
            cell.lbTop.text = listConsonants1[indexPath.row].letterExamplesOriginalVoice
            cell.lbMid.text = listConsonants1[indexPath.row].letterExamplesOriginal
    //        cell.lbBot.text = listConsonantsCourses[indexPath.row].DetailTranscriptionText
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SpeakingCell", for: indexPath) as! SpeakingCell
        cell.lbTop.text = listSpokenCourses[indexPath.row].DetailVoice
        cell.lbMid.text = listSpokenCourses[indexPath.row].DetailOriginalText
        cell.lbBot.text = listSpokenCourses[indexPath.row].DetailTranscriptionText
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexSelect != indexPath.row{
            indexSelect = indexPath.row
            SpeakingCollectionView.reloadData()
        }
        indexSelect = indexPath.row
        SpeakingCollectionView.reloadData()
    }
    
}
extension SpeakingViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexSelect == indexPath.row{
            if UIDevice.current.userInterfaceIdiom == .pad {
                return CGSize(width: self.SpeakingCollectionView.frame.width/1.2, height: 200)
            }else{
                return CGSize(width: self.SpeakingCollectionView.frame.width/1.2, height: 180)
            }
        }
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: self.SpeakingCollectionView.frame.width/1.2, height: 105)
        }else{
            return CGSize(width: self.SpeakingCollectionView.frame.width/1.2, height: 105)
        }
    }
}
