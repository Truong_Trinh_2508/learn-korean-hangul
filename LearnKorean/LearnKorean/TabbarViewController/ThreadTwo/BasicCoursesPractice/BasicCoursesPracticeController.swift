//
//  BasicCoursesPracticeController.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 24/11/2021.
//

import UIKit
import RxSwift

class BasicCoursesPracticeController: UIViewController {
    var titlePrac: String?
    var imgTitle:String?
    var titlePracLetter:String?
    var index:Int?
    @IBOutlet weak var Quick: UIView!
    @IBOutlet weak var Text: UIView!
    @IBOutlet weak var Matching: UIView!
    @IBOutlet weak var Speaking: UIView!
    @IBOutlet weak var imgPractice: UIImageView!
    @IBOutlet weak var titlePractice: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        titlePractice.text = titlePrac
        imgPractice.image = UIImage(named: imgTitle ?? "")
        Configue()
    }
    func Configue(){
        Quick.applyShadow()
        Quick.border(color: .toolbarColor, width: 1*heightRatio)
        Quick.circle()
        Text.applyShadow()
        Text.border(color: .toolbarColor, width: 1*heightRatio)
        Text.circle()
        Matching.applyShadow()
        Matching.border(color: .toolbarColor, width: 1*heightRatio)
        Matching.circle()
        Speaking.applyShadow()
        Speaking.border(color: .toolbarColor, width: 1*heightRatio)
        Speaking.circle()

    }
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnAcQuick(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "QuickViewController") as! QuickViewController
        vc.contentCourses = titlePracLetter
        vc.indexpro = index
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func btnAcMatching(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MatchingViewController") as! MatchingViewController
        vc.contentCourses = titlePracLetter
        vc.indexpro = index
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }

    @IBAction func btnAcSpeaking(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SpeakingViewController") as! SpeakingViewController
        vc.contentCourses = titlePracLetter
        vc.index = index
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func btnAcTextReading(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TextReadingViewController") as! TextReadingViewController
        vc.contentCourses = titlePracLetter
        vc.index = index
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
