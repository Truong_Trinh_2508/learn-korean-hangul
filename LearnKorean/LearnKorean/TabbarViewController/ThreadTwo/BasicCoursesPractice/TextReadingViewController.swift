//
//  TextReadingViewController.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 29/11/2021.
//

import UIKit

class TextReadingViewController: UIViewController {
    var listSpokenCourses:[DailyPhrasesModel] = [DailyPhrasesModel]()
    var listDaiyPhrases:[DailyPhrasesModel] = [DailyPhrasesModel]()
    var listConsonants:[LettersModel] = [LettersModel]()
    var listConsonants1:[LettersModel] = [LettersModel]()
    var contentCourses:String!
    var index:Int!
    var indexSelect = -1
    @IBOutlet weak var TextCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getDataSpokenCourses(){_,_ in}
        TextCollectionView.register(UINib(nibName: TextReadingCell.className, bundle: nil), forCellWithReuseIdentifier: TextReadingCell.className)
        listConsonants = SQLiteService.shared.getConsonantsCourses(type: self.contentCourses)
        for i in listConsonants{
            if i.letterExamplesOriginal != ""{
                listConsonants1.append(i)
            }
        }
    }
    func getDataSpokenCourses(andCompletion completion:@escaping(_ moviesResponse: [DailyPhrasesModel], _ error: Error?) -> ()){
        SQLiteService.shared.DailyPhrases(){ repond,error in
            if let repond = repond{
                self.listDaiyPhrases = repond
                self.TextCollectionView.reloadData()
            }
        }
        for item in listDaiyPhrases{
            if item.UnitName == contentCourses {
                listSpokenCourses.append(item)
            }
        }
    }
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension TextReadingViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if index == 0 {
            return listConsonants1.count
        }
        return listSpokenCourses.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if index == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TextReadingCell", for: indexPath) as! TextReadingCell
            cell.lbTop.text = listConsonants1[indexPath.row].letterExamplesOriginalVoice
            cell.lbMid.text = listConsonants1[indexPath.row].letterExamplesOriginal
    //        cell.lbBot.text = listConsonantsCourses[indexPath.row].DetailTranscriptionText
            cell.backgroundColor = .white
            cell.applyShadow()
            cell.border(color: .toolbarColor, width: 1*heightRatio)
            cell.circle()
//            if indexSelect == indexPath.row{
//                TextToSpeech.shared.readText(text: listConsonants1[indexPath.row].letterExamplesOriginal)
//            }
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TextReadingCell", for: indexPath) as! TextReadingCell
        cell.lbTop.text = listSpokenCourses[indexPath.row].DetailVoice
        cell.lbMid.text = listSpokenCourses[indexPath.row].DetailOriginalText
        cell.lbBot.text = listSpokenCourses[indexPath.row].DetailTranscriptionText
        cell.backgroundColor = .white
        cell.circle()
        cell.border(color: .toolbarColor, width: 1*heightRatio)
        cell.applyShadow()
//        if indexSelect == indexPath.row{
//            TextToSpeech.shared.readText(text: listSpokenCourses[indexPath.row].DetailOriginalText)
//        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if indexSelect != indexPath.row{
//            indexSelect = indexPath.row
//            TextCollectionView.reloadData()
//        }
        indexSelect = indexPath.row
        TextCollectionView.reloadData()
    }
    
}
extension TextReadingViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: self.TextCollectionView.frame.width/1.2, height: 100)
        }else{
            return CGSize(width: self.TextCollectionView.frame.width/1.2, height: 80)
        }
    }
}
