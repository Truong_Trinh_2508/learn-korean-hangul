//
//  TextReadingCell.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 29/11/2021.
//

import UIKit

class TextReadingCell: UICollectionViewCell {

    @IBOutlet weak var lbBot: UILabel!
    @IBOutlet weak var lbMid: UILabel!
    @IBOutlet weak var lbTop: UILabel!
    @IBOutlet weak var ContainerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
//        ContainerView.circle()
//        ContainerView.applyShadow()
    }

    @IBAction func btnAcSpeak(_ sender: Any) {
        TextToSpeech.shared.readText(text: lbMid.text)
    }
}
