//
//  SpeakingCell.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 03/12/2021.
//

import UIKit

class SpeakingCell: UICollectionViewCell {
    var check:Bool = true
    @IBOutlet weak var btnPlaySpeak: UIButton!
    @IBOutlet weak var btnReplay: UIButton!
    @IBOutlet weak var btnRecord: UIButton!
    @IBOutlet weak var lbBot: UILabel!
    @IBOutlet weak var lbMid: UILabel!
    @IBOutlet weak var lbTop: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func btnAcReplay(_ sender: Any) {
        let indexpro = AKAudioRecorder.shared.getIndex
        if indexpro >= 1{
            let name = AKAudioRecorder.shared.getRecordings[indexpro - 1]
            AKAudioRecorder.shared.play(name: name)
        }
        AKAudioRecorder.shared.play(name: "")
    }
    @IBAction func btnAcRecord(_ sender: Any) {
        if AKAudioRecorder.shared.isRecording{
            btnRecord.setBackgroundImage(UIImage(named: "record") ,for : UIControl.State.normal)
            AKAudioRecorder.shared.stopRecording()
        } else{
            btnRecord.setBackgroundImage(UIImage(named: "pause") ,for : UIControl.State.normal)
            AKAudioRecorder.shared.record()
        }
    }
    @IBAction func btnAcPlaySpeak(_ sender: Any) {
        TextToSpeech.shared.readText(text: lbMid.text ?? "heello word")
    }
}
