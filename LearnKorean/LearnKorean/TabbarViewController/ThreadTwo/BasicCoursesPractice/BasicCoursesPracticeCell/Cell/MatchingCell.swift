//
//  MatchingCell.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 03/12/2021.
//

import UIKit

class MatchingCell: UICollectionViewCell {

    @IBOutlet weak var imgMatching: UIImageView!
    @IBOutlet weak var lbWords: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func prepareForReuse() {
        self.imgMatching.image = UIImage(named: "UnChoose")
    }
}
