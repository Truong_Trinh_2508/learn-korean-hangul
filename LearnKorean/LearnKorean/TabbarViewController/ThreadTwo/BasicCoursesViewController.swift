//
//  BaiscCoursesViewController.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 17/11/2021.
//

import UIKit

class BasicCoursesViewController: UIViewController {
 
    var bRec:Bool = false
    var bRec1:Bool = false
    var bRec2:Bool = false
    var bRec3:Bool = false
    var index = 0
    var listCategoryCourses:[LettersModel] = [LettersModel]()
    var listCategoryLetters:[LettersModel] = [LettersModel]()
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var TabbarView: UIView!
    @IBOutlet weak var btnLetters: UIButton!
    @IBOutlet weak var btnBasicCourses: UIButton!
    @IBOutlet weak var btnCourses: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: SpokenCoursesCell.className, bundle: nil), forCellWithReuseIdentifier: SpokenCoursesCell.className)
        Configue()
        listCategoryCourses = SQLiteService.shared.getCategoryCourses(type: "c")
    }
    private func Configue(){
        TabbarView.applyShadow()
        TabbarView.border(color: .tabbarColor, width: 1*heightRatio)
        TabbarView.circle()
    }
    
    @IBAction func btnAcLetters(_ sender: Any) {
        if bRec == true {
            btnLetters.setImage(UIImage(named: "UnLetters"), for: .normal)
            bRec = false
        }else{
            btnLetters.setImage(UIImage(named: "Letters"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: LettersController.className) as? LettersController
            vc!.modalPresentationStyle = .fullScreen
            self.present(vc!, animated: false, completion: nil)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            btnBasicCourses.setImage(UIImage(named: "UnBasicCourses"), for: .normal)
            btnCourses.setImage(UIImage(named: "UnCourses"), for: .normal)
            btnProfile.setImage(UIImage(named: "UnProfile"), for: .normal)
        }
    }
    @IBAction func btnAcBasicCourses(_ sender: Any) {
        if bRec1 == true {
            btnBasicCourses.setImage(UIImage(named: "UnBasicCourses"), for: .normal)
            bRec1 = false
        }else{
            btnBasicCourses.setImage(UIImage(named: "BasicCourses"), for: .normal)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            btnLetters.setImage(UIImage(named: "UnLetters"), for: .normal)
            btnCourses.setImage(UIImage(named: "UnCourses"), for: .normal)
            btnProfile.setImage(UIImage(named: "UnProfile"), for: .normal)
        }
    }
    @IBAction func btnAcCourses(_ sender: Any) {
        if bRec2 == true {
            btnCourses.setImage(UIImage(named: "UnCourses"), for: .normal)
            bRec2 = false
        }else{
            btnCourses.setImage(UIImage(named: "Courses"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: CoursesViewController.className) as? CoursesViewController
            vc!.modalPresentationStyle = .fullScreen
            self.present(vc!, animated: false, completion: nil)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            btnBasicCourses.setImage(UIImage(named: "UnBasicCourses"), for: .normal)
            btnLetters.setImage(UIImage(named: "UnLetters"), for: .normal)
            btnProfile.setImage(UIImage(named: "UnProfile"), for: .normal)
        }
    }
    @IBAction func btnAcProfile(_ sender: Any) {
        if bRec3 == true {
            btnProfile.setImage(UIImage(named: "UnProfile"), for: .normal)
            bRec3 = false
        }else{
            btnProfile.setImage(UIImage(named: "Profile"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: ProfileViewController.className) as? ProfileViewController
            vc!.modalPresentationStyle = .fullScreen
            self.present(vc!, animated: false, completion: nil)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            btnBasicCourses.setImage(UIImage(named: "UnBasicCourses"), for: .normal)
            btnCourses.setImage(UIImage(named: "UnCourses"), for: .normal)
            btnLetters.setImage(UIImage(named: "UnLetters"), for: .normal)
        }
    }
}
extension BasicCoursesViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listCategoryCourses.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SpokenCoursesCell", for: indexPath) as! SpokenCoursesCell
        cell.titleSpoken.text = listCategoryCourses[indexPath.row].letter
        cell.imageTitle.image = UIImage(named: listCategoryCourses[indexPath.row].letter)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BasicCoursesPracticeController") as! BasicCoursesPracticeController
        vc.imgTitle = listCategoryCourses[indexPath.row].letter
        vc.titlePrac = listCategoryCourses[indexPath.row].letter
        vc.titlePracLetter = listCategoryCourses[indexPath.row].letterFVoice
        vc.index = index
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true)
    }
}
extension BasicCoursesViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: self.collectionView.frame.width/4, height: 150)
        }else{
            return CGSize(width: self.collectionView.frame.width/4, height: 120)
        }
    }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 20, left: 15, bottom: 10, right: 15)
        }
}
