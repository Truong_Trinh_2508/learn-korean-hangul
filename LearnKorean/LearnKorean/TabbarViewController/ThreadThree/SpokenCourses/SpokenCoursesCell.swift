//
//  SpokenCoursesCell.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 26/11/2021.
//

import UIKit

class SpokenCoursesCell: UICollectionViewCell {

    @IBOutlet weak var imageTitle: UIImageView!
    @IBOutlet weak var titleSpoken: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
