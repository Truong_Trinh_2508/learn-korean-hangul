//
//  CoursesViewController.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 17/11/2021.
//

import UIKit
import RxSwift

final class CoursesViewController: BaseViewController {
    var listTitleUnit:[String] = ["Unit 1","Unit 2","Unit 3","Unit 4","Unit 5","Unit 6"]
    var arrUnit:[[LessonsCatalogueModel]] = [[LessonsCatalogueModel]()]
    var listSpoken:[DailyPhrasesModel] = [DailyPhrasesModel]()
    var listCategorySpoken:[DailyPhrasesModel] = [DailyPhrasesModel]()
    var bRec:Bool = false
    var bRec1:Bool = false
    var bRec2:Bool = false
    var bRec3:Bool = false
    var index:Int = 0
    var indexDidSelect:Int = 1
    var listUnit:[LessonsCatalogueModel] = [LessonsCatalogueModel]()
    var listUnit1:[LessonsCatalogueModel] = [LessonsCatalogueModel]()
    var listUnit2:[LessonsCatalogueModel] = [LessonsCatalogueModel]()
    var listUnit3:[LessonsCatalogueModel] = [LessonsCatalogueModel]()
    var listUnit4:[LessonsCatalogueModel] = [LessonsCatalogueModel]()
    var listUnit5:[LessonsCatalogueModel] = [LessonsCatalogueModel]()
    var listUnit6:[LessonsCatalogueModel] = [LessonsCatalogueModel]()
    @IBOutlet weak var btnSpoken: UIButton!
    @IBOutlet weak var btnGrammar: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var toolbarView: UIView!
    @IBOutlet weak var TabbarView: UIView!
    @IBOutlet weak var btnLetters: UIButton!
    @IBOutlet weak var btnBasicCourses: UIButton!
    @IBOutlet weak var btnCourses: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getDataCourses(){_,_ in}
        self.getDataSpokenCourses(){_,_ in}
        arrUnit = [listUnit1,listUnit2,listUnit3,listUnit4,listUnit5,listUnit6]
        print(listUnit1)
        collectionView.register(UINib(nibName: GrammarCoursesCell.className, bundle: nil), forCellWithReuseIdentifier: GrammarCoursesCell.className)
        collectionView.register(UINib(nibName: SpokenCoursesCell.className, bundle: nil), forCellWithReuseIdentifier: SpokenCoursesCell.className)
    }
    func getDataCourses(andCompletion completion:@escaping(_ moviesResponse: [LessonsCatalogueModel], _ error: Error?) -> ()){
        SQLiteService.shared.Catalogue(){ repond,error in
            if let repond = repond{
                self.listUnit = repond
                self.collectionView.reloadData()
            }
        }
        for item in listUnit{
            if item.chapter == "Unit1"{
                listUnit1.append(item)
            }
        }
        for item in listUnit{
            if item.chapter == "Unit2"{
                listUnit2.append(item)
            }
        }
        for item in listUnit{
            if item.chapter == "Unit3"{
                listUnit3.append(item)
            }
        }
        for item in listUnit{
            if item.chapter == "Unit4"{
                listUnit4.append(item)
            }
        }
        for item in listUnit{
            if item.chapter == "Unit5"{
                listUnit5.append(item)
            }
        }
        for item in listUnit{
            if item.chapter == "Unit6"{
                listUnit6.append(item)
            }
        }
    }
    func getDataSpokenCourses(andCompletion completion:@escaping(_ moviesResponse: [DailyPhrasesModel], _ error: Error?) -> ()){
        SQLiteService.shared.DailyPhrases(){ result,error in
            if let result = result{
                self.listSpoken = result
                self.collectionView.reloadData()
            }
        }
        for a in listSpoken{
            if a.key == 1{
                listCategorySpoken.append(a)
            }
        }
    }
    override func setupUI() {
        btnSpoken.border(color: .toolbarColor, width: 1*heightRatio)
        btnSpoken.circle()
        btnSpoken.applyShadow()
        btnGrammar.border(color: .toolbarColor, width: 1*heightRatio)
        btnGrammar.circle()
        btnGrammar.applyShadow()
        TabbarView.applyShadow()
        TabbarView.border(color: .tabbarColor, width: 1*heightRatio)
        TabbarView.circle()
    }
    
    @IBAction func btnAcLetters(_ sender: Any) {
        if bRec == true {
            btnLetters.setImage(UIImage(named: "UnLetters"), for: .normal)
            bRec = false
        }else{
            btnLetters.setImage(UIImage(named: "Letters"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: LettersController.className) as? LettersController
            vc!.modalPresentationStyle = .fullScreen
            self.present(vc!, animated: false, completion: nil)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            btnBasicCourses.setImage(UIImage(named: "UnBasicCourses"), for: .normal)
            btnCourses.setImage(UIImage(named: "UnCourses"), for: .normal)
            btnProfile.setImage(UIImage(named: "UnProfile"), for: .normal)
        }
    }
    @IBAction func btnAcBasicCourses(_ sender: Any) {
        if bRec1 == true {
            btnBasicCourses.setImage(UIImage(named: "UnBasicCourses"), for: .normal)
            bRec1 = false
        }else{
            btnBasicCourses.setImage(UIImage(named: "BasicCourses"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: BasicCoursesViewController.className) as? BasicCoursesViewController
            vc!.modalPresentationStyle = .fullScreen
            self.present(vc!, animated: false, completion: nil)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            btnLetters.setImage(UIImage(named: "UnLetters"), for: .normal)
            btnCourses.setImage(UIImage(named: "UnCourses"), for: .normal)
            btnProfile.setImage(UIImage(named: "UnProfile"), for: .normal)
        }
    }
    @IBAction func btnAcCourses(_ sender: Any) {
        if bRec2 == true {
            btnCourses.setImage(UIImage(named: "UnCourses"), for: .normal)
            bRec2 = false
        }else{
            btnCourses.setImage(UIImage(named: "Courses"), for: .normal)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            btnBasicCourses.setImage(UIImage(named: "UnBasicCourses"), for: .normal)
            btnLetters.setImage(UIImage(named: "UnLetters"), for: .normal)
            btnProfile.setImage(UIImage(named: "UnProfile"), for: .normal)
        }
    }
    @IBAction func btnAcProfile(_ sender: Any) {
        if bRec3 == true {
            btnProfile.setImage(UIImage(named: "UnProfile"), for: .normal)
            bRec3 = false
        }else{
            btnProfile.setImage(UIImage(named: "Profile"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: ProfileViewController.className) as? ProfileViewController
            vc!.modalPresentationStyle = .fullScreen
            self.present(vc!, animated: false, completion: nil)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            btnBasicCourses.setImage(UIImage(named: "UnBasicCourses"), for: .normal)
            btnCourses.setImage(UIImage(named: "UnCourses"), for: .normal)
            btnLetters.setImage(UIImage(named: "UnLetters"), for: .normal)
        }
    }
    
    @IBAction func btnAcGrammar(_ sender: Any) {
        index = 1
        btnGrammar.do{
            $0.backgroundColor = .toolbarColor
            $0.setTitleColor(.white, for: .normal)
        }
        btnSpoken.do{
            $0.backgroundColor = .white
            $0.setTitleColor(.toolbarColor, for: .normal)
        }
        collectionView.reloadData()
    }
    @IBAction func btnAcSpoken(_ sender: Any) {
        index = 0
        btnSpoken.do{
            $0.backgroundColor = .toolbarColor
            $0.setTitleColor(.white, for: .normal)
        }
        btnGrammar.do{
            $0.backgroundColor = .white
            $0.setTitleColor(.toolbarColor, for: .normal)
        }
        collectionView.reloadData()
    }
}
extension CoursesViewController: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if index == 0{
            return listCategorySpoken.count
        }
        return 6
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if index == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SpokenCoursesCell", for: indexPath) as! SpokenCoursesCell
            cell.titleSpoken.text = listCategorySpoken[indexPath.row].UnitName
            cell.imageTitle.image = UIImage(named: listCategorySpoken[indexPath.row].UnitName)
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GrammarCoursesCell", for: indexPath) as! GrammarCoursesCell
        cell.dataInMain(with: arrUnit[indexPath.row])
        cell.lbUnit.text = listTitleUnit[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if index == 0{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BasicCoursesPracticeController") as! BasicCoursesPracticeController
            vc.imgTitle = listCategorySpoken[indexPath.row].UnitName
            vc.titlePrac = listCategorySpoken[indexPath.row].UnitName
            vc.titlePracLetter = listCategorySpoken[indexPath.row].UnitName
            vc.index = indexDidSelect
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true)
        }
        
    }
    
}
extension CoursesViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if index == 0{
            if UIDevice.current.userInterfaceIdiom == .pad {
                return CGSize(width: self.collectionView.frame.width/3.1 , height: 150)
            }else{
                return CGSize(width: self.collectionView.frame.width/3.1, height: 120)
            }
        }
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: self.collectionView.frame.width , height: 800)
        }else{
            return CGSize(width: self.collectionView.frame.width, height: 650)
        }
    }
}
