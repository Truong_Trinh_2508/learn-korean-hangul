//
//  GrammarCoursesCell.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 26/11/2021.
//

import UIKit

class GrammarCoursesCell: UICollectionViewCell {
    var arr : [LessonsCatalogueModel] = []
    @IBOutlet weak var lbUnit: UILabel!
    @IBOutlet weak var GrammarCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        GrammarCollectionView.register(UINib(nibName: GrammarChildrenCell.className, bundle: nil), forCellWithReuseIdentifier: GrammarChildrenCell.className)
        GrammarCollectionView.delegate = self
        GrammarCollectionView.dataSource = self
    }
    func dataInMain(with arr :[LessonsCatalogueModel] ){
        self.arr = arr
        GrammarCollectionView.reloadData()
    }
    
    
}
extension GrammarCoursesCell:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GrammarChildrenCell.className, for: indexPath) as! GrammarChildrenCell
        cell.imgTitle.image = UIImage.init(named: arr[indexPath.row].lesson_dialog_tag)
        cell.titleGrammar.text = arr[indexPath.row].lesson_title
        cell.grammar.text = arr[indexPath.row].lesson_grammar_title
        cell.backgroundColor = .white
        cell.circle()
        cell.applyShadow()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let parentVC = self.parentViewController as? CoursesViewController{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DiaVoViewController") as! DiaVoViewController
            vc.lessonDialogueTag = arr[indexPath.row].lesson_dialog_tag
            vc.TitleGrammar = arr[indexPath.row].lesson_title
            vc.modalPresentationStyle = .fullScreen
            parentVC.present(vc, animated: true)
        }
    }
    
}
extension GrammarCoursesCell:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 30
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: self.GrammarCollectionView.frame.width/1.2, height: self.GrammarCollectionView.frame.height/6.5)
        }else{
            return CGSize(width: self.GrammarCollectionView.frame.width/1.2, height: self.GrammarCollectionView.frame.height/6.5)
        }
    }
}
