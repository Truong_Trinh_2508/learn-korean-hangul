//
//  DiaVoViewController.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 01/12/2021.
//

import UIKit

class DiaVoViewController: UIViewController {
    var listDialogue:[LessonDialogueModel] = [LessonDialogueModel]()
    var listDialogueLesson:[LessonDialogueModel] = [LessonDialogueModel]()
    var listWords:[LessonWordsModel] = [LessonWordsModel]()
    var listWordsLesson:[LessonWordsModel] = [LessonWordsModel]()
    @IBOutlet weak var DiaVoCollectionView: UICollectionView!
    @IBOutlet weak var DialogueView: UIView!
    @IBOutlet weak var VocabularyView: UIView!
    @IBOutlet weak var btnVocabulary: UIButton!
    @IBOutlet weak var btnDialogue: UIButton!
    @IBOutlet weak var titleGrammar: UILabel!
    var index:Int = 0
    var lessonDialogueTag:String!
    var TitleGrammar:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getDataDialogue(){_,_ in}
        self.getDataWords(){_,_ in}
        titleGrammar.text = TitleGrammar
        DiaVoCollectionView.register(UINib(nibName: DialogueCell.className, bundle: nil), forCellWithReuseIdentifier: DialogueCell.className)
    }
    func getDataDialogue(andCompletion completion:@escaping(_ moviesResponse: [LessonDialogueModel], _ error: Error?) -> ()){
        SQLiteService.shared.Dialogue(){ result,error in
            if let result = result{
                self.listDialogue = result
                self.DiaVoCollectionView.reloadData()
            }
        }
        for a in listDialogue{
            if a.lesson_dialog_tag == lessonDialogueTag{
                listDialogueLesson.append(a)
            }
        }
        
    }
    func getDataWords(andCompletion completion:@escaping(_ moviesResponse: [LessonWordsModel], _ error: Error?) -> ()){
        SQLiteService.shared.Words(){ result,error in
            if let result = result{
                self.listWords = result
                self.DiaVoCollectionView.reloadData()
            }
        }
        for a in listWords{
            if a.lesson_words_tag == lessonDialogueTag{
                listWordsLesson.append(a)
            }
        }
    }
    @IBAction func btnAcVocabulary(_ sender: Any) {
        index = 1
        DialogueView.backgroundColor = .clear
        VocabularyView.backgroundColor = .tabbarColor
        btnVocabulary.do{
            $0.setTitleColor(.tabbarColor, for: .normal)
        }
        btnDialogue.do{
            $0.setTitleColor(.black, for: .normal)
        }
        DiaVoCollectionView.reloadData()
    }
    @IBAction func btnAcDialogue(_ sender: Any) {
        index = 0
        DialogueView.backgroundColor = .tabbarColor
        VocabularyView.backgroundColor = .clear
        btnVocabulary.do{
            $0.setTitleColor(.black, for: .normal)
        }
        btnDialogue.do{
            $0.setTitleColor(.tabbarColor, for: .normal)
        }
        DiaVoCollectionView.reloadData()
    }
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension DiaVoViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if index == 0{
            return listDialogueLesson.count
        }
        return listWordsLesson.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if index == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DialogueCell.className, for: indexPath) as! DialogueCell
            cell.lbTop.text = listDialogueLesson[indexPath.row].dialog_translate_text
            cell.lbmid.text = listDialogueLesson[indexPath.row].dialog_original_text
            cell.lbBot.text = listDialogueLesson[indexPath.row].dialog_transliterate_text
            cell.lbFlag.text = listDialogueLesson[indexPath.row].role_flag
            cell.circle()
            cell.applyShadow()
            return cell
            
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DialogueCell.className, for: indexPath) as! DialogueCell
        cell.lbTop.text = listWordsLesson[indexPath.row].word_translate_text
        cell.lbmid.text = listWordsLesson[indexPath.row].word_original_text
        cell.lbBot.text = listWordsLesson[indexPath.row].word_transliterate_text
        cell.circle()
        cell.applyShadow()
        return cell
    }
    
    
}
extension DiaVoViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: self.DiaVoCollectionView.frame.width/1.1 , height: 120)
        }else{
            return CGSize(width: self.DiaVoCollectionView.frame.width/1.1, height: 90)
        }
    }
}
