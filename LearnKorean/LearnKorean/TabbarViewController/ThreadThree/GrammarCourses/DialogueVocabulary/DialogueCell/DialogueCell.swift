//
//  DialogueCell.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 02/12/2021.
//

import UIKit

class DialogueCell: UICollectionViewCell {

    @IBOutlet weak var lbFlag: UILabel!
    @IBOutlet weak var lbBot: UILabel!
    @IBOutlet weak var lbmid: UILabel!
    @IBOutlet weak var lbTop: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func btnTextSpeak(_ sender: Any) {
        TextToSpeech1.shared.readText(text: lbmid.text)
    }
}
