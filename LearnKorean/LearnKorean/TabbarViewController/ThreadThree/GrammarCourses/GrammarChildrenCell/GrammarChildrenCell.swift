//
//  GrammarChildrenCell.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 26/11/2021.
//

import UIKit

class GrammarChildrenCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var grammar: UILabel!
    @IBOutlet weak var titleGrammar: UILabel!
    @IBOutlet weak var imgTitle: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        imgTitle.circle()
        }
}
