//
//  LetterCell.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 19/11/2021.
//

import UIKit

class LetterCell: UICollectionViewCell {

    @IBOutlet weak var lbLetter: UILabel!
    @IBOutlet weak var ContainerLetterView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
