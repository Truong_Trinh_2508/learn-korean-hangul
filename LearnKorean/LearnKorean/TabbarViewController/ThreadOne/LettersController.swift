//
//  ViewController.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 17/11/2021.
//

import UIKit


final class LettersController: BaseViewController {
    
    var ListLetters: [LettersModel] = [LettersModel]()
    var bRec:Bool = false
    var bRec1:Bool = false
    var bRec2:Bool = false
    var bRec3:Bool = false
    var index :Int = 0
    var listDataLettersAG = [LettersModel]()
    var listDataLettersAeG = [LettersModel]()
    
    //    @IBOutlet weak var CategoryCollectionView: UICollectionView!
    @IBOutlet private weak var btnHangul: UIButton!
    @IBOutlet private weak var btnAeG: UIButton!
    @IBOutlet private weak var btnAG: UIButton!
    @IBOutlet private weak var LettersCollectionView: UICollectionView!
    @IBOutlet private weak var ContentView: UIView!
    @IBOutlet private weak var TabbarView: UIView!
    @IBOutlet private weak var btnLetters: UIButton!
    @IBOutlet private weak var btnBasicCourses: UIButton!
    @IBOutlet private weak var btnCourses: UIButton!
    @IBOutlet private weak var btnProfile: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        LettersCollectionView.register(UINib(nibName: LetterCell.className, bundle: nil), forCellWithReuseIdentifier: LetterCell.className)
        LettersCollectionView.register(UINib(nibName: HangulCell.className, bundle: nil), forCellWithReuseIdentifier: HangulCell.className)
        LettersCollectionView.register(UINib(nibName: VowelsCell.className, bundle: nil), forCellWithReuseIdentifier: VowelsCell.className)
        LettersCollectionView.register(UINib(nibName: ConsonantsCell.className, bundle: nil), forCellWithReuseIdentifier: ConsonantsCell.className)
        Configue()
        self.getDataLetters(){_,_ in}
    }
    func getDataLetters(andCompletion completion:@escaping(_ moviesResponse: [LettersModel], _ error: Error?) -> ()){
        SQLiteService.shared.getDataLetters(){ repond,error in
            if let repond = repond{
                self.ListLetters = repond
                self.LettersCollectionView.reloadData()
            }
        }
        for item in 0...218{
            listDataLettersAG.append(ListLetters[item])
        }
        for item1 in 219...457{
            listDataLettersAeG.append(ListLetters[item1])
        }
        listDataLettersAG.insert(LettersModel(id: 0, letter: "", letterImage: "", letterSpell: "", letterFVoice: "", letterSVoice: "", letterVoice: "", letterExamplesOriginal: "", letterExamplesOriginalVoice: ""), at: 0)
        listDataLettersAeG.insert(LettersModel(id: 219, letter: "", letterImage: "", letterSpell: "", letterFVoice: "", letterSVoice: "", letterVoice: "", letterExamplesOriginal: "", letterExamplesOriginalVoice: ""), at: 0)
    }
    
    private func Configue(){
        TabbarView.applyShadow()
        TabbarView.border(color: .tabbarColor, width: 1*heightRatio)
        TabbarView.circle()
        btnAG.border(color: .toolbarColor, width: 1*heightRatio)
        btnAG.circle()
        btnAG.applyShadow()
        btnAeG.border(color: .toolbarColor, width: 1*heightRatio)
        btnAeG.circle()
        btnAeG.applyShadow()
        btnHangul.border(color: .toolbarColor, width: 1*heightRatio)
        btnHangul.circle()
        btnHangul.applyShadow()
    }
    @IBAction func btnAcLetters(_ sender: Any) {
        if bRec == true {
            btnLetters.setImage(UIImage(named: "UnLetters"), for: .normal)
            bRec = false
        }else{
            btnLetters.setImage(UIImage(named: "Letters"), for: .normal)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            btnBasicCourses.setImage(UIImage(named: "UnBasicCourses"), for: .normal)
            btnCourses.setImage(UIImage(named: "UnCourses"), for: .normal)
            btnProfile.setImage(UIImage(named: "UnProfile"), for: .normal)
        }
    }
    @IBAction func btnAcBasicCourses(_ sender: Any) {
        if bRec1 == true {
            btnBasicCourses.setImage(UIImage(named: "UnBasicCourses"), for: .normal)
            bRec1 = false
        }else{
            btnBasicCourses.setImage(UIImage(named: "BasicCourses"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: BasicCoursesViewController.className) as? BasicCoursesViewController
            vc!.modalPresentationStyle = .fullScreen
            self.present(vc!, animated: false, completion: nil)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            btnLetters.setImage(UIImage(named: "UnLetters"), for: .normal)
            btnCourses.setImage(UIImage(named: "UnCourses"), for: .normal)
            btnProfile.setImage(UIImage(named: "UnProfile"), for: .normal)
        }
    }
    @IBAction func btnAcCourses(_ sender: Any) {
        if bRec2 == true {
            btnCourses.setImage(UIImage(named: "UnCourses"), for: .normal)
            bRec2 = false
        }else{
            btnCourses.setImage(UIImage(named: "Courses"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: CoursesViewController.className) as? CoursesViewController
            vc!.modalPresentationStyle = .fullScreen
            self.present(vc!, animated: false, completion: nil)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            btnBasicCourses.setImage(UIImage(named: "UnBasicCourses"), for: .normal)
            btnLetters.setImage(UIImage(named: "UnLetters"), for: .normal)
            btnProfile.setImage(UIImage(named: "UnProfile"), for: .normal)
        }
    }
    @IBAction func btnAcProfile(_ sender: Any) {
        if bRec3 == true {
            btnProfile.setImage(UIImage(named: "UnProfile"), for: .normal)
            bRec3 = false
        }else{
            btnProfile.setImage(UIImage(named: "Profile"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: ProfileViewController.className) as? ProfileViewController
            vc!.modalPresentationStyle = .fullScreen
            self.present(vc!, animated: false, completion: nil)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            btnBasicCourses.setImage(UIImage(named: "UnBasicCourses"), for: .normal)
            btnCourses.setImage(UIImage(named: "UnCourses"), for: .normal)
            btnLetters.setImage(UIImage(named: "UnLetters"), for: .normal)
        }
    }
    
    @IBAction func btn1(_ sender: Any) {
        index = 0
        btnAG.do{
            $0.backgroundColor = .toolbarColor
            $0.setTitleColor(.white, for: .normal)
        }
        btnAeG.do{
            $0.backgroundColor = .white
            $0.setTitleColor(.toolbarColor, for: .normal)
        }
        btnHangul.do{
            $0.backgroundColor = .white
            $0.setTitleColor(.toolbarColor, for: .normal)
        }
        LettersCollectionView.reloadData()
    }
    @IBAction func btn2(_ sender: Any) {
        index = 1
        btnAG.do{
            $0.backgroundColor = .white
            $0.setTitleColor(.toolbarColor, for: .normal)
        }
        btnAeG.do{
            $0.backgroundColor = .toolbarColor
            $0.setTitleColor(.white, for: .normal)
        }
        btnHangul.do{
            $0.backgroundColor = .white
            $0.setTitleColor(.toolbarColor, for: .normal)
        }
        LettersCollectionView.reloadData()
    }
    @IBAction func btn3(_ sender: Any) {
        index = 2
        btnAeG.do{
            $0.backgroundColor = .white
            $0.setTitleColor(.toolbarColor, for: .normal)
        }
        btnHangul.do{
            $0.backgroundColor = .toolbarColor
            $0.setTitleColor(.white, for: .normal)
        }
        btnAG.do{
            $0.backgroundColor = .white
            $0.setTitleColor(.toolbarColor, for: .normal)
        }
        LettersCollectionView.reloadData()
    }
}
extension LettersController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if index == 0{
            return 1
        }
        else if index == 1{
            return 1
        }
        else{
            return 3
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if index == 0{
            return  listDataLettersAG.count
        }
        else if index == 1 {
            return  listDataLettersAeG.count
        }
        else{
            if section == 0{
                return 1
            }
            else if section == 1{
                return 1
            }
            else{
                return 1
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if index == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LetterCell", for: indexPath) as! LetterCell
            cell.lbLetter.text = listDataLettersAG[indexPath.row].letter
            cell.backgroundColor = .white
            return cell
        }
        else if index == 1{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LetterCell", for: indexPath) as! LetterCell
            cell.lbLetter.text = listDataLettersAeG[indexPath.row].letter
            cell.backgroundColor = .white
            return cell
        }
        if indexPath.section == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HangulCell", for: indexPath) as! HangulCell
            return cell
        }
        else if indexPath.section == 1{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VowelsCell", for: indexPath) as! VowelsCell
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ConsonantsCell", for: indexPath) as! ConsonantsCell
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopUpController") as! PopUpController
        if index == 0{
            TextToSpeech.shared.readText(text: listDataLettersAG[indexPath.row].letter)
            vc.letter = listDataLettersAG[indexPath.row].letter
            vc.LetterLatin = listDataLettersAG[indexPath.row].letterImage
            vc.LetterSpell = listDataLettersAG[indexPath.row].letterSpell
            vc.LetterExOriginal = listDataLettersAG[indexPath.row].letterExamplesOriginal
            vc.LetterExTrans = listDataLettersAG[indexPath.row].letterExamplesOriginalVoice
            self.present(vc, animated: true)
        }
        else if index == 1{
            TextToSpeech.shared.readText(text: listDataLettersAeG[indexPath.row].letter)
            vc.letter = listDataLettersAeG[indexPath.row].letter
            vc.LetterLatin = listDataLettersAeG[indexPath.row].letterImage
            vc.LetterSpell = listDataLettersAeG[indexPath.row].letterSpell
            vc.LetterExOriginal = listDataLettersAeG[indexPath.row].letterExamplesOriginal
            vc.LetterExTrans = listDataLettersAeG[indexPath.row].letterExamplesOriginalVoice
            self.present(vc, animated: true)
        }
        
    }
}
extension LettersController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if index == 0{
            if UIDevice.current.userInterfaceIdiom == .pad {
                return CGSize(width: self.LettersCollectionView.frame.width/12, height: 55)
            }else{
                return CGSize(width: self.LettersCollectionView.frame.width/13, height: 30)
            }
        }
        else if index == 1{
            if UIDevice.current.userInterfaceIdiom == .pad {
                return CGSize(width: self.LettersCollectionView.frame.width/13, height: 50)
            }else{
                return CGSize(width: self.LettersCollectionView.frame.width/14, height: 30)
            }
        }
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: self.LettersCollectionView.frame.width , height: 450)
        }else{
            return CGSize(width: self.LettersCollectionView.frame.width, height: 300)
        }
    }
}
