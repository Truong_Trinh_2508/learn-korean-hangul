//
//  ConsonantsCell.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 25/11/2021.
//

import UIKit

class ConsonantsCell: UICollectionViewCell {
    var listConsonants:[LettersModel] = [LettersModel]()
    @IBOutlet weak var ConsonantsCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        ConsonantsCollectionView.register(UINib(nibName: VoConCell.className, bundle: nil), forCellWithReuseIdentifier: VoConCell.className)
        ConsonantsCollectionView.delegate = self
        ConsonantsCollectionView.dataSource = self
        listConsonants = SQLiteService.shared.getDataVoCon(type: "c")
    }
}
extension ConsonantsCell : UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listConsonants.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VoConCell", for: indexPath) as! VoConCell
        cell.ImageVoCon.image = UIImage(named: listConsonants[indexPath.row].letterImage)
        return cell
    }
}
extension ConsonantsCell:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: self.ConsonantsCollectionView.frame.width/3.1 , height: 120)
        }else{
            return CGSize(width: self.ConsonantsCollectionView.frame.width/3.1, height: 120)
        }
    }

}
