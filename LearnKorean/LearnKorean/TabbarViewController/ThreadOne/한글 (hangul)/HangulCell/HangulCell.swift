//
//  HangulCell.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 25/11/2021.
//

import UIKit

class HangulCell: UICollectionViewCell {
    var listTitle:[String] = ["History of Hangul","The principles of creating Hangul","Final Consonants","Hangul Syllable Blocks"]
    var listContent:[String] = ["Until the 15th century Korean was written...","Letters in the Hangul alphabet are called j...","“Batchim” is the consonant places under th...","Hangul has 40 letters (19 consonants and..."]
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var HangulCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        HangulCollectionView.register(UINib(nibName: HangulChilrenCell.className, bundle: nil), forCellWithReuseIdentifier: HangulChilrenCell.className)
        HangulCollectionView.delegate = self
        HangulCollectionView.dataSource = self
    }
}
extension HangulCell : UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listTitle.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HangulChilrenCell", for: indexPath) as! HangulChilrenCell
        cell.lbTitle.text = listTitle[indexPath.row]
        cell.lbContent.text = listContent[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let parentVC = self.parentViewController as? LettersController{
            if indexPath.item == 0{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "HangulHistoryViewController") as! HangulHistoryViewController
                vc.modalPresentationStyle = .fullScreen
                parentVC.present(vc, animated: true)
            }
        }
    }
}
extension HangulCell:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 40
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: self.HangulCollectionView.frame.width/2.3 , height: self.HangulCollectionView.frame.height/3 + 20)
        }else{
            return CGSize(width: self.HangulCollectionView.frame.width/2.3, height: self.HangulCollectionView.frame.height/3 + 10)
        }
    }
    
}
