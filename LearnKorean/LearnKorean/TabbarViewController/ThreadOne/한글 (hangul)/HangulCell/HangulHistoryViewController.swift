//
//  HangulHistoryViewController.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 29/11/2021.
//

import UIKit

class HangulHistoryViewController: UIViewController {
    @IBOutlet weak var ContentTextView: UITextView!
    var content:String = "Until the 15th century Korean was written entirely using chinese characters.Sine this required learning several thousand complicated characters only those with lots od time and money dor education were literate and only a tiny number of aristocrats were able to read and write fluently.\n In 1446 the fourth king of the Joseon Dynasty, Sejong the Great, published a document called Hunmin Jeong-eum Eonhae that described a new alphabet (Hangul) that was specifically designed to be easy to learn."
    override func viewDidLoad() {
        super.viewDidLoad()
        ContentTextView.text = content
    }
    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
