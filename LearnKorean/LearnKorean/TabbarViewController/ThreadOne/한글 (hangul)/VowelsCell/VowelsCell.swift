//
//  VowelsCell.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 25/11/2021.
//

import UIKit

class VowelsCell: UICollectionViewCell {
    var listVowels:[LettersModel] = [LettersModel]()
    @IBOutlet weak var VowelsTitle: UILabel!
    @IBOutlet weak var VowelsCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        VowelsCollectionView.register(UINib(nibName: VoConCell.className, bundle: nil), forCellWithReuseIdentifier: VoConCell.className)
        VowelsCollectionView.delegate = self
        VowelsCollectionView.dataSource = self
        listVowels = SQLiteService.shared.getDataVoCon(type: "v")
    }
}
extension VowelsCell : UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listVowels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VoConCell", for: indexPath) as! VoConCell
        cell.ImageVoCon.image = UIImage(named: listVowels[indexPath.row].letterImage)
        return cell
    }
}
extension VowelsCell:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: self.VowelsCollectionView.frame.width/3.1 , height: 120)
        }else{
            return CGSize(width: self.VowelsCollectionView.frame.width/3.1 , height: 120)
        }
    }

}
