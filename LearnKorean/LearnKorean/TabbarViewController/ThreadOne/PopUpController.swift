//
//  PopUpController.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 24/11/2021.
//

import UIKit

class PopUpController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lbLetterExTrans: UILabel!
    @IBOutlet weak var lbLetterExOriginal: UILabel!
    @IBOutlet weak var btnListening: UIButton!
    @IBOutlet weak var lbLetterSpell: UILabel!
    @IBOutlet weak var lbLetterLatin: UILabel!
    @IBOutlet weak var lbLetter: UILabel!
    var letter:String?
    var LetterLatin:String?
    var LetterSpell:String?
    var LetterExOriginal:String?
    var LetterExTrans:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView.applyShadow()
        containerView.popupRadius()
        lbLetter.text = letter
        lbLetterLatin.text = LetterLatin
        lbLetterSpell.text = LetterSpell
        lbLetterExOriginal.text = LetterExOriginal
        lbLetterExTrans.text = LetterExTrans
    }

    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func TextSpeak(_ sender: Any) {
        TextToSpeech.shared.readText(text: LetterExOriginal)
    }
    
}
