//
//  ProfileCell.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 23/11/2021.
//

import UIKit

class ProfileCell: UICollectionViewCell {

    @IBOutlet weak var language: UILabel!
    @IBOutlet weak var Container: UIView!
    @IBOutlet weak var lbTitleProfile: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configui()
    }
    private func configui(){
        Container.border(color: .toolbarColor, width: 1*heightRatio)
        Container.circle()
        lbTitleProfile.labelShadow()
    }
}
