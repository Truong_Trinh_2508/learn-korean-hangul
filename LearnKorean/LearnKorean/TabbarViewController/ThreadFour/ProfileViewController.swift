//
//  ProfileViewController.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 17/11/2021.
//

import UIKit

class ProfileViewController: UIViewController {
    var listProfile:[String] = ["About Us","Share this app","Rate app","Give feedback","Privacy policy"]
    var bRec:Bool = false
    var bRec1:Bool = false
    var bRec2:Bool = false
    var bRec3:Bool = false
    var language:String?
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var TabbarView: UIView!
    @IBOutlet weak var btnLetters: UIButton!
    @IBOutlet weak var btnBasicCourses: UIButton!
    @IBOutlet weak var btnCourses: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: ProfileCell.className, bundle: nil), forCellWithReuseIdentifier: ProfileCell.className)
        Configue()
    }
    private func Configue(){
        TabbarView.applyShadow()
        TabbarView.border(color: .tabbarColor, width: 1*heightRatio)
        TabbarView.circle()
    }
    
    @IBAction func btnAcLetters(_ sender: Any) {
        if bRec == true {
            btnLetters.setImage(UIImage(named: "UnLetters"), for: .normal)
            bRec = false
        }else{
            btnLetters.setImage(UIImage(named: "Letters"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: LettersController.className) as? LettersController
            vc!.modalPresentationStyle = .fullScreen
            self.present(vc!, animated: false, completion: nil)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            btnBasicCourses.setImage(UIImage(named: "UnBasicCourses"), for: .normal)
            btnCourses.setImage(UIImage(named: "UnCourses"), for: .normal)
            btnProfile.setImage(UIImage(named: "UnProfile"), for: .normal)
        }
    }
    @IBAction func btnAcBasicCourses(_ sender: Any) {
        if bRec1 == true {
            btnBasicCourses.setImage(UIImage(named: "UnBasicCourses"), for: .normal)
            bRec1 = false
        }else{
            btnBasicCourses.setImage(UIImage(named: "BasicCourses"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: BasicCoursesViewController.className) as? BasicCoursesViewController
            vc!.modalPresentationStyle = .fullScreen
            self.present(vc!, animated: false, completion: nil)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            btnLetters.setImage(UIImage(named: "UnLetters"), for: .normal)
            btnCourses.setImage(UIImage(named: "UnCourses"), for: .normal)
            btnProfile.setImage(UIImage(named: "UnProfile"), for: .normal)
        }
    }
    @IBAction func btnAcCourses(_ sender: Any) {
        if bRec2 == true {
            btnCourses.setImage(UIImage(named: "UnCourses"), for: .normal)
            bRec2 = false
        }else{
            btnCourses.setImage(UIImage(named: "Courses"), for: .normal)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: CoursesViewController.className) as? CoursesViewController
            vc!.modalPresentationStyle = .fullScreen
            self.present(vc!, animated: false, completion: nil)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            btnBasicCourses.setImage(UIImage(named: "UnBasicCourses"), for: .normal)
            btnLetters.setImage(UIImage(named: "UnLetters"), for: .normal)
            btnProfile.setImage(UIImage(named: "UnProfile"), for: .normal)
        }
    }
    @IBAction func btnAcProfile(_ sender: Any) {
        if bRec3 == true {
            btnProfile.setImage(UIImage(named: "UnProfile"), for: .normal)
            bRec3 = false
        }else{
            btnProfile.setImage(UIImage(named: "Profile"), for: .normal)
            bRec1 = false
            bRec = false
            bRec2 = false
            bRec3 = false
            btnBasicCourses.setImage(UIImage(named: "UnBasicCourses"), for: .normal)
            btnCourses.setImage(UIImage(named: "UnCourses"), for: .normal)
            btnLetters.setImage(UIImage(named: "UnLetters"), for: .normal)
        }
    }
}
extension ProfileViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
        cell.lbTitleProfile.text = listProfile[indexPath.row]
        cell.language.text = language
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listProfile.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return CGSize(width: self.collectionView.bounds.width,height: 80)
        }else{
            return CGSize(width: self.collectionView.bounds.width,height: 60)
        }
    }
}
