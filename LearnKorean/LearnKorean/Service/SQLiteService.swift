//
//  SQLiteService.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 17/11/2021.
//

import UIKit
import Foundation
import SQLite

extension FileManager {
    func copyfileToUserDocumentDirectory(forResource name: String,
                                         ofType ext: String) throws -> String
    {
        if let bundlePath = Bundle.main.path(forResource: name, ofType: ext),
           let destPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                              .userDomainMask,
                                                              true).first {
            let fileName = "\(name).\(ext)"
            let fullDestPath = URL(fileURLWithPath: destPath)
                .appendingPathComponent(fileName)
            let fullDestPathString = fullDestPath.path
            
            if !self.fileExists(atPath: fullDestPathString) {
                try self.copyItem(atPath: bundlePath, toPath: fullDestPathString)
                
            }
            return fullDestPathString
        }
        return ""
    }
}


class SQLiteService: NSObject {
    static let shared : SQLiteService = SQLiteService()
    public let connection:Connection?
    var DataLetters = [LettersModel]()
    var DataDailyPhrases = [DailyPhrasesModel]()
    var DataDialogue = [LessonDialogueModel]()
    var DataGrammar = [LessonGrammarModel]()
    var DataCatalogue = [LessonsCatalogueModel]()
    var DataWords = [LessonWordsModel]()
    var DataVideoPhrases = [VideoPhrasesModel]()
    override init() {
        do {
            let fileManager = FileManager.default
            let dbPath = try fileManager.copyfileToUserDocumentDirectory(forResource: "ThaiNgu1", ofType: "db")
            connection = try Connection ("\(dbPath)")
        }catch{
            connection = nil
            let nserror = error as NSError
            print("Cannot connect to Databace. Error is: \(nserror), \(nserror.userInfo)")
        }
    }
    
    // MARK: getDataLetters
    /// getDataLetters
    /// - Parameter closure: LettersModel
    func getDataLetters(closure: @escaping (_ response: [LettersModel]?, _ error: Error?) -> Void) {
        let users = Table("LettersModel")
        let id = Expression<Int>("id")
        let letter = Expression<String>("letter")
        let letterImage = Expression<String>("letterImage")
        let letterSpell = Expression<String>("letterSpell")
        let letterFVoice = Expression<String?>("letterFVoice")
        let letterSVoice = Expression<String?>("letterSVoice")
        let letterVoice = Expression<String?>("letterVoice")
        let letterExamplesOriginal = Expression<String?>("letterExamplesOriginal")
        let letterExamplesOriginalVoice = Expression<String?>("letterExamplesOriginalVoice")
        
        DataLetters.removeAll()
        if let DatabaseRoot = connection {
            do{
                for user in try DatabaseRoot.prepare(users) {
                    DataLetters.append(LettersModel(id: Int(user[id]),
                                                    letter: user[letter],
                                                    letterImage: user[letterImage],
                                                    letterSpell: user[letterSpell],
                                                    letterFVoice: user[letterFVoice] ?? "",
                                                    letterSVoice: user[letterSVoice] ?? "",
                                                    letterVoice: user[letterVoice] ?? "",
                                                    letterExamplesOriginal: user[letterExamplesOriginal] ?? "",
                                                    letterExamplesOriginalVoice: user[letterExamplesOriginalVoice] ?? ""))
                }
            }catch{
                print("Error!")
                return
            }
        }
        closure(DataLetters, nil)
    }
    func getCategoryCourses(type:String) -> [LettersModel]{
        var listCategoryCouses = [LettersModel]()
        for item in DataLetters{
            if item.letterSVoice == type && item.letterExamplesOriginal != ""{
                listCategoryCouses.append(item)
            }
        }
        return listCategoryCouses
    }

    func getDataVoCon(type:String) -> [LettersModel]{
        var listDataVoCon = [LettersModel]()
        for item in DataLetters{
            if item.letterSVoice == type{
                listDataVoCon.append(item)
            }
        }
        return listDataVoCon
    }
    func getConsonantsCourses(type:String) -> [LettersModel]{
        var listConsonantsCourses = [LettersModel]()
        for item in DataLetters{
            if item.letterFVoice == type{
                listConsonantsCourses.append(item)
            }
        }
        return listConsonantsCourses
    }
    
    // MARK: getDailyPhrases
    /// DailyPhrases
    /// ham nay de get data
    /// - Parameter closure: DailyPhrasesModel
    func DailyPhrases(closure: @escaping (_ response: [DailyPhrasesModel]?, _ error: Error?) -> Void) {
        let users = Table("DailyPhrasesModel")
        let id = Expression<Int>("id")
        let DetailVoice = Expression<String?>("DetailVoice")
        let DetailOriginalText = Expression<String?>("DetailOriginalText")
        let DetailTranslateText = Expression<String?>("DetailTranslateText")
        let DetailTranscriptionText = Expression<String?>("DetailTranscriptionText")
        let UnitName = Expression<String?>("UnitName")
        let key = Expression<Int?>("key")
        
        DataDailyPhrases.removeAll()
        if let DatabaseRoot = connection {
            do{
                for user in try DatabaseRoot.prepare(users) {
                    DataDailyPhrases.append(DailyPhrasesModel(id: Int(user[id]),
                                                              DetailVoice: user[DetailVoice] ?? "",
                                                              DetailOriginalText: user[DetailOriginalText] ?? "",
                                                              DetailTranslateText: user[DetailTranslateText] ?? "",
                                                              DetailTranscriptionText: user[DetailTranscriptionText] ?? "",
                                                              UnitName: user[UnitName] ?? "",
                                                              key: Int(user[key] ?? 0)))
                }
            }catch{
                print("Error!")
                return
            }
        }
        closure(DataDailyPhrases, nil)
    }
    func getSpokenCourses(type:String) -> [DailyPhrasesModel]{
        var listSpokenCourses = [DailyPhrasesModel]()
        for item in DataDailyPhrases{
            if item.UnitName == type{
                listSpokenCourses.append(item)
            }
        }
        return listSpokenCourses
    }
    
    // MARK: getCatalogue
    /// Catalogue
    /// ham nay de get data
    /// - Parameter closure: LessonsCatalogueModel
    func Catalogue(closure: @escaping (_ response: [LessonsCatalogueModel]?, _ error: Error?) -> Void) {
        let users = Table("LessonsCatalogueModel")
        let id = Expression<Int?>("id")
        let chapter = Expression<String?>("chapter")
        let lesson_title = Expression<String?>("lesson_title")
        let lesson_grammar_title = Expression<String?>("lesson_grammar_title")
        let lesson_dialog_tag = Expression<String?>("lesson_dialog_tag")
        
        DataCatalogue.removeAll()
        if let DatabaseRoot = connection {
            do{
                for user in try DatabaseRoot.prepare(users) {
                    if let id = user[id], let chapter = user[chapter], let lesson_title = user[lesson_title], let lesson_grammar_title = user[lesson_grammar_title], let lesson_dialog_tag = user[lesson_dialog_tag]{
                        let valueModelLetters = LessonsCatalogueModel(id: id, chapter: chapter, lesson_title: lesson_title, lesson_grammar_title: lesson_grammar_title, lesson_dialog_tag: lesson_dialog_tag)
                        DataCatalogue.append(valueModelLetters)
                    }else{
                        let valueModelLetters = LessonsCatalogueModel(id: 0, chapter: "", lesson_title: "", lesson_grammar_title: "", lesson_dialog_tag: "")
                        DataCatalogue.append(valueModelLetters)
                    }
                    
                }
            }catch{
                print("Error!")
                return
            }
        }
        closure(DataCatalogue, nil)
    }
    func getDataUnit() -> [LessonsCatalogueModel]{
        var listDataUnit = [LessonsCatalogueModel]()
        for item in DataCatalogue{
            if item.chapter == "Unit1"{
                listDataUnit.append(item)
            }
        }
        return listDataUnit
    }
    
    // MARK: getDialogue
    /// Dialogue
    /// ham nay de get data
    /// - Parameter closure: LessonDialogueModel
    func Dialogue(closure: @escaping (_ response: [LessonDialogueModel]?, _ error: Error?) -> Void) {
        let users = Table("LessonDialogueModel")
        let id = Expression<Int>("id")
        let lesson_dialog_tag = Expression<String?>("lesson_dialog_tag")
        let dialog_order = Expression<String?>("dialog_order")
        let dialog_original_text = Expression<String?>("dialog_original_text")
        let dialog_start_time = Expression<String?>("dialog_start_time")
        let dialog_end_time = Expression<String?>("dialog_end_time")
        let dialog_translate_text = Expression<String?>("dialog_translate_text")
        let dialog_transliterate_text = Expression<String?>("dialog_transliterate_text")
        let role_flag = Expression<String?>("role_flag")
        
        DataDialogue.removeAll()
        if let DatabaseRoot = connection {
            do{
                for user in try DatabaseRoot.prepare(users) {
                    DataDialogue.append(LessonDialogueModel(id: Int(user[id]), lesson_dialog_tag: user[lesson_dialog_tag] ?? "", dialog_order: user[dialog_order] ?? "", dialog_original_text: user[dialog_original_text] ?? "", dialog_start_time: user[dialog_start_time] ?? "", dialog_end_time: user[dialog_end_time] ?? "", dialog_translate_text: user[dialog_translate_text] ?? "", dialog_transliterate_text: user[dialog_transliterate_text] ?? "", role_flag: user[role_flag] ?? ""))
                }
            }catch{
                print("Error!")
                return
            }
        }
        closure(DataDialogue, nil)
    }
    // MARK: getWords
    /// Words
    /// ham nay de get data
    /// - Parameter closure: LessonWordsModel
    func Words(closure: @escaping (_ response: [LessonWordsModel]?, _ error: Error?) -> Void) {
        let users = Table("LessonWordsModel")
        let id = Expression<Int>("id")
        let lesson_words_tag = Expression<String?>("lesson_words_tag")
        let word_order = Expression<String?>("word_order")
        let word_original_text = Expression<String?>("word_original_text")
        let word_start_time = Expression<String?>("word_start_time")
        let word_translate_text = Expression<String?>("word_translate_text")
        let word_transliterate_text = Expression<String?>("word_transliterate_text")
        let word_end_time = Expression<String?>("word_end_time")
        
        DataWords.removeAll()
        if let DatabaseRoot = connection {
            do{
                for user in try DatabaseRoot.prepare(users) {
                    DataWords.append(LessonWordsModel(id: Int(user[id]),
                                                      lesson_words_tag: user[lesson_words_tag] ?? "",
                                                      word_order: user[word_order] ?? "",
                                                      word_original_text: user[word_original_text] ?? "",
                                                      word_start_time: user[word_start_time] ?? "",
                                                      word_translate_text: user[word_translate_text] ?? "",
                                                      word_transliterate_text: user[word_transliterate_text] ?? "",
                                                      word_end_time: user[word_end_time] ?? ""))
                }
            }catch{
                print("Error!")
                return
            }
        }
        closure(DataWords, nil)
    }
}
