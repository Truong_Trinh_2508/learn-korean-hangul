//
//  DailyPhrasesModel.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 17/11/2021.
//

import UIKit
import SQLite

class DailyPhrasesModel {
    var id: Int = 0
    var DetailVoice:String = ""
    var DetailOriginalText:String = ""
    var DetailTranslateText:String = ""
    var DetailTranscriptionText:String = ""
    var UnitName:String = ""
    var key:Int = 0
    
    init(id:Int,DetailVoice:String,DetailOriginalText:String,DetailTranslateText:String,DetailTranscriptionText:String,UnitName:String,key:Int){
        self.id = id
        self.DetailVoice = DetailVoice
        self.DetailOriginalText = DetailOriginalText
        self.DetailTranslateText = DetailTranslateText
        self.DetailTranscriptionText = DetailTranscriptionText
        self.UnitName = UnitName
        self.key = key
    }
}
