//
//  VideoPhrasesModel.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 17/11/2021.
//

import UIKit
import SQLite

class VideoPhrasesModel{
    var id :Int = 0
    var DetailVideo:String = ""
    var DetailImage:String = ""
    var DetailKoreanText:String = ""
    var DetailTranslateText:String = ""
    var DetailTranscriptionText:String = ""
    var UnitName:String = ""
    init(id:Int,DetailVideo:String,DetailImage:String,DetailKoreanText:String,DetailTranslateText:String,DetailTranscriptionText:String,UnitName:String){
        self.id = id
        self.DetailVideo = DetailVideo
        self.DetailImage = DetailImage
        self.DetailKoreanText = DetailKoreanText
        self.DetailTranslateText = DetailTranslateText
        self.DetailTranscriptionText = DetailTranscriptionText
        self.UnitName = UnitName
    }
}
