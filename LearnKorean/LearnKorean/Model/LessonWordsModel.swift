//
//  LessonWordsModel.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 17/11/2021.
//

import UIKit
import SQLite

class LessonWordsModel: NSObject {
    var id:Int = 0
    var lesson_words_tag:String = ""
    var word_order:String = ""
    var word_original_text:String = ""
    var word_start_time:String = ""
    var word_translate_text:String = ""
    var word_transliterate_text:String = ""
    var word_end_time:String = ""
    init(id:Int,lesson_words_tag:String,word_order:String,word_original_text:String,word_start_time:String,word_translate_text:String,word_transliterate_text:String,word_end_time:String) {
        self.id = id
        self.lesson_words_tag = lesson_words_tag
        self.word_order = word_order
        self.word_original_text = word_original_text
        self.word_start_time = word_start_time
        self.word_translate_text = word_translate_text
        self.word_transliterate_text = word_transliterate_text
        self.word_end_time = word_end_time
    }
}
