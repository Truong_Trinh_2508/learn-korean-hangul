//
//  LessonGrammarModel.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 17/11/2021.
//

import UIKit
import SQLite

class LessonGrammarModel{
    var id :Int = 0
    var lesson_grammar_tag:String = ""
    var grammar_order:String = ""
    var grammar_title:String = ""
    var grammar_desc:String = ""
    var grammar_ex_original_text1:String = ""
    var grammar_ex_translate_text1:String = ""
    var grammar_ex_transliterate_text1:String = ""
    var grammar_ex_original_text2:String = ""
    var grammar_ex_translate_text2:String = ""
    var grammar_ex_transliterate_text2:String = ""
    init(id:Int,lesson_grammar_tag:String,grammar_order:String,grammar_title:String,grammar_desc:String,grammar_ex_original_text1:String,grammar_ex_translate_text1:String,grammar_ex_transliterate_text1:String,grammar_ex_original_text2:String,grammar_ex_translate_text2:String,grammar_ex_transliterate_text2:String){
        self.id = id
        self.lesson_grammar_tag = lesson_grammar_tag
        self.grammar_order = grammar_order
        self.grammar_title = grammar_title
        self.grammar_desc = grammar_desc
        self.grammar_ex_original_text1 = grammar_ex_original_text1
        self.grammar_ex_translate_text1 = grammar_ex_translate_text1
        self.grammar_ex_transliterate_text1 = grammar_ex_transliterate_text1
        self.grammar_ex_original_text2 = grammar_ex_original_text2
        self.grammar_ex_translate_text2 = grammar_ex_translate_text2
        self.grammar_ex_transliterate_text2 = grammar_ex_transliterate_text2
        
    }
}
