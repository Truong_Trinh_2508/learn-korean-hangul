//
//  LessonDialogueModel.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 17/11/2021.
//

import UIKit
import SQLite

class LessonDialogueModel{
    var id:Int = 0
    var lesson_dialog_tag:String  = ""
    var dialog_order:String = ""
    var dialog_original_text:String = ""
    var dialog_start_time:String = ""
    var dialog_end_time:String = ""
    var dialog_translate_text:String = ""
    var dialog_transliterate_text:String = ""
    var role_flag:String = ""
    init(id:Int,lesson_dialog_tag:String,dialog_order:String,dialog_original_text:String,dialog_start_time:String,dialog_end_time:String,dialog_translate_text:String,dialog_transliterate_text:String,role_flag:String){
        self.id = id
        self.lesson_dialog_tag = lesson_dialog_tag
        self.dialog_order = dialog_order
        self.dialog_original_text = dialog_original_text
        self.dialog_start_time = dialog_start_time
        self.dialog_translate_text = dialog_translate_text
        self.dialog_transliterate_text = dialog_transliterate_text
        self.role_flag = role_flag
    
    }
}



