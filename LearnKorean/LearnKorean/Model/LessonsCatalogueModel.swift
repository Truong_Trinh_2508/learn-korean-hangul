//
//  LessonsCatalogueModel.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 17/11/2021.
//

import UIKit
import SQLite

class LessonsCatalogueModel{
    var id :Int = 0
    var chapter:String = ""
    var lesson_title:String = ""
    var lesson_grammar_title:String = ""
    var lesson_dialog_tag:String = ""

    init(id :Int,chapter:String,lesson_title:String,lesson_grammar_title:String,lesson_dialog_tag:String){
        self.id = id
        self.chapter = chapter
        self.lesson_title = lesson_title
        self.lesson_grammar_title = lesson_grammar_title
        self.lesson_dialog_tag = lesson_dialog_tag
        
    }
}
