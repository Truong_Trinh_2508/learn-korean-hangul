//
//  LettersModel.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 17/11/2021.
//

import UIKit
import Foundation

class LettersModel {
    var id:Int = 0
    var letter:String = ""
    var letterImage:String = ""
    var letterSpell:String = ""
    var letterFVoice:String = ""
    var letterSVoice:String = ""
    var letterVoice:String = ""
    var letterExamplesOriginal:String = ""
    var letterExamplesOriginalVoice:String = ""

    init(id:Int,letter:String ,letterImage:String,letterSpell:String,letterFVoice:String,letterSVoice:String,letterVoice:String,letterExamplesOriginal:String,letterExamplesOriginalVoice:String) {
        self.id = id
        self.letter = letter
        self.letterImage = letterImage
        self.letterSpell = letterSpell
        self.letterFVoice = letterFVoice
        self.letterSVoice = letterSVoice
        self.letterVoice = letterVoice
        self.letterExamplesOriginal = letterExamplesOriginal
        self.letterExamplesOriginalVoice = letterExamplesOriginalVoice
    }
}
