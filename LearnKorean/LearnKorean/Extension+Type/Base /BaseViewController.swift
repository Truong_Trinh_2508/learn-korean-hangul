//
//  BaseViewController.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 18/11/2021.
//

import UIKit
import RxSwift
import PanModal

class BaseViewController: UIViewController {

    // MARK: - Public Properties
    internal var bag = DisposeBag()

    internal var isShowBack = true {
        didSet {
            configBackButton()
        }
    }

    internal var backButtonTintColor: UIColor = .white {
        didSet {
            backButton.tintColor = backButtonTintColor
        }
    }

    internal var tintColorNavigation: UIColor = .black

    // MARK: - Private Properties
    private lazy var backButton = UIButton().with {
        $0.imageView?.contentMode = .scaleAspectFit
        $0.imageEdgeInsets = UIEdgeInsets(top: 10, left: -5, bottom: 10, right: 25)
        $0.setImage(#imageLiteral(resourceName: "ic_back.pdf"), for: .normal)
        $0.tintColor = .black
        $0.addTarget(self, action: #selector(self.backTapped), for: .touchUpInside)
    }

    // MARK: - Override
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    override var prefersStatusBarHidden: Bool {
        return false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        defaultStyle()
        setupUI()
        bindRxOutlets()
        setupData()
    }

//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        configNavigationBar()
//    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: tintColorNavigation]
    }

    // MARK: - Public Functions
    internal func setupUI() {}

    internal func bindRxOutlets() {}

    internal func setupData() {}

    func presentPanModalBase(vc: PanModalPresentable.LayoutType) {
        if isIPad {
            self.presentPanModal(vc, sourceView: self.view, sourceRect: CGRect(x: 0,y: self.view.bounds.midY,width: 0,height: 0))
        } else {
            self.presentPanModal(vc)
        }
    }

    deinit {
        debugPrint("\(String(describing: type(of: self))) \(#function)")
        NotificationCenter.default.removeObserver(self)
    }
}

// MARK: - Private Functions
extension BaseViewController {
    private func defaultStyle() {
        view.backgroundColor = UIColor.white

    }

//    private func configNavigationBar() {
//        navigationItem.title = title
//    }

    private func configBackButton() {
        if isShowBack {
            navigationItem.backBarButtonItem = UIBarButtonItem(
                title: nil,
                style: .plain,
                target: nil,
                action: nil)
            let spaceButton = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
            let backBarButton = UIBarButtonItem(customView: backButton)
            navigationItem.leftBarButtonItems = [spaceButton, backBarButton]
        } else {
            navigationItem.leftBarButtonItem = nil
            navigationItem.setHidesBackButton(true, animated: true)
        }
    }

    @objc func backTapped() {
        navigationController?.popViewController(animated: true)
    }

    private func handlePresentAlert(_ alertVC: UIViewController) {
        alertVC.modalTransitionStyle = .crossDissolve
        alertVC.modalPresentationStyle = .overCurrentContext
        navigationController?.present(alertVC, animated: true)
    }
}

// MARK: - Public Functions
extension BaseViewController {

}
