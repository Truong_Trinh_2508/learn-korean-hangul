//
//  CategoryType.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 18/11/2021.
//

import UIKit
import Foundation

enum CategoryType: String, CaseIterable {
    case Level1 = "ㅏ~ ㅡ"
    case Level2 = "ㅐ~ ㅢ"
    case Hangul = "한글(Hangul)"
}

enum TabbarType: String, CaseIterable{
    case Letters =  "Letters"
    case BasicCourses =  "BasicCourses"
    case Courses =  "Courses"
    case Profile =  "Profile"
}
enum CategoryCoursesType: String {
    case SpokenCourses = "SpokenCourses"
    case GrammarCourses = "GrammarCourses"
}
