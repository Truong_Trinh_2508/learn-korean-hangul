//
//  Extension.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 18/11/2021.
//

import UIKit

let isIPad = DeviceType.IS_IPAD
let heightRatio = screenSize.height/896
let widthRatio = screenSize.width/414

extension NSObject {
    func background(delay: Double = 0.0, background: (()->Void)? = nil, completion: (() -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async {
            background?()
            if let completion = completion {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                    completion()
                })
            }
        }
    }
    var className: String {
        return String(describing: type(of: self))
    }
    class var className: String {
        return String(describing: self)
    }
}
extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
extension UILabel{
    func labelShadow(
        color: UIColor = .black,
                        alpha: Float = 0.25,
                        x: CGFloat = 0,
                        y: CGFloat = 3,
                        blur: CGFloat = 8) {
            layer.shadowColor = color.cgColor
            layer.shadowOffset = CGSize(width: x, height: y)
            layer.shadowOpacity = alpha
            layer.shadowRadius = blur
            layer.masksToBounds = false
    }
}
extension UIView {
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue*heightRatio
            layer.masksToBounds = newValue > 0
        }
    }
    
    func border(color: UIColor, width: CGFloat) {
        layer.borderColor = color.cgColor
        layer.borderWidth = width*heightRatio
        layer.cornerRadius = corner
        layer.masksToBounds = true
    }
    
    var corner: CGFloat {
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = true
        }
        get {
            return layer.cornerRadius
        }
    }
    func applyShadow(
        color: UIColor = .black,
                        alpha: Float = 0.25,
                        x: CGFloat = 0,
                        y: CGFloat = 3,
                        blur: CGFloat = 8) {
            layer.shadowColor = color.cgColor
            layer.shadowOffset = CGSize(width: x, height: y)
            layer.shadowOpacity = alpha
            layer.shadowRadius = blur
            layer.masksToBounds = false
    }
    func circle() {
        layoutIfNeeded()
        layer.cornerRadius = min(bounds.width, bounds.height) / 3
        layer.masksToBounds = true
    }
    func popupRadius(){
        layoutIfNeeded()
        layer.cornerRadius = min(bounds.width, bounds.height) / 4
        layer.masksToBounds = true
    }
}
extension UIColor {
    static var cell : UIColor{
        return hex("")
    }
    static var tabbarColor: UIColor {
        return hex("C6A600")
    }
    static var toolbarColor: UIColor {
        return hex("FFDC98")
    }
    static var mainColor: UIColor {
        return hex("FF5151")
    }
    
    static var mainLighterColor: UIColor {
        return hex("FFF4F4")
    }
    
    static var hex_FEC8C8: UIColor {
        return hex("FEC8C8")
    }
    
    static var hex_FCC4C4: UIColor {
        return hex("FCC4C4")
    }
    
    static var hex_F3F3F3: UIColor {
        return hex("F3F3F3")
    }
    
    static var hex_6A7BDE: UIColor {
        return hex("6A7BDE")
    }

    
    static var lightCoral: UIColor {
        return hex("FF8282")
    }
    
    static var nero: UIColor {
        return hex("242424")
    }
    
    static var wePeep: UIColor {
        return hex("FED3D5")
    }
    
    static var none: UIColor {
        return hex("E0E0E0")
    }
    
    static var mediumBlue: UIColor {
        return hex("606FEC")
    }
    
    static var hex_F0F0F0: UIColor {
        return hex("F0F0F0")
    }

    static func rgb(red: Int, green: Int, blue: Int, alpha: CGFloat = 1.0) -> UIColor {
        let denominator: CGFloat = 255.0
        let color = UIColor(red: CGFloat(red) / denominator,
                            green: CGFloat(green) / denominator,
                            blue: CGFloat(blue) / denominator,
                            alpha: alpha)
        return color
    }
    
    static func hex(_ hexStr: String, alpha: CGFloat = 1) -> UIColor {
        let scanner = Scanner(string: hexStr.replacingOccurrences(of: "#", with: ""))
        var color: UInt32 = 0
        guard scanner.scanHexInt32(&color) else {
            return .white
        }
        let red = CGFloat((color & 0xFF0000) >> 16) / 255.0
        let green = CGFloat((color & 0x00FF00) >> 8) / 255.0
        let blue = CGFloat(color & 0x0000FF) / 255.0
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    var hexString: String {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0
        
        getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        let rgb: Int = (Int)(red * 255) << 16 | (Int)(green * 255) << 8 | (Int)(blue * 255) << 0
        
        return String(format: "#%06x", rgb)
    }
}
extension UITabBarController {
    func setTabBarHidden(_ isHidden: Bool, animated: Bool, completion: (() -> Void)? = nil) {
        if tabBar.isHidden == isHidden {
            completion?()
        }

        if !isHidden {
            tabBar.isHidden = false
        }

        let height = tabBar.frame.size.height
        let offsetY = view.frame.height - (isHidden ? 0 : height)
        let duration = (animated ? 0.3 : 0.0)

        let frame = CGRect(origin: CGPoint(x: tabBar.frame.minX, y: offsetY), size: tabBar.frame.size)
        UIView.animate(withDuration: duration, animations: {
            self.tabBar.frame = frame
        }, completion: { _ in
                self.tabBar.isHidden = isHidden
                completion?()
            })
    }
}
//extension SpeakingCell{
//    func animate(isRecording : Bool){
//        if isRecording{
//        self.btnRecord.layer.cornerRadius = 4
//        UIView.animate(withDuration: 0.1) {
//            self.btnRecord.transform = CGAffineTransform(scaleX: 2, y: 2)
//            self.btnRecord.layer.cornerRadius = 15
//        }
//        }
//        else{
//            UIView.animate(withDuration: 0.1) {
//                self.btnRecord.transform = .identity
//                self.btnRecord.layer.cornerRadius = 4
//            }
//        }
//    }
//
//}
