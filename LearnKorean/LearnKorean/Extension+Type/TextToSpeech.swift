//
//  TextToSpeech.swift
//  LearnKorean
//
//  Created by Trịnh Trường on 24/11/2021.
//

import UIKit
import AVFoundation
class TextToSpeech {
    static let shared = TextToSpeech()
    func readText(text: String?){
        let utterance = AVSpeechUtterance(string: text ?? "")
        utterance.voice = AVSpeechSynthesisVoice(language: "ko-KR")
        utterance.rate = 0.1
        let synthesizer = AVSpeechSynthesizer()
        synthesizer.speak(utterance)
    }
}
class TextToSpeech1 {
    static let shared = TextToSpeech1()
    func readText(text: String?){
        let utterance = AVSpeechUtterance(string: text ?? "")
        utterance.voice = AVSpeechSynthesisVoice(language: "ko-KR")
        utterance.rate = 0.35
        let synthesizer = AVSpeechSynthesizer()
        synthesizer.speak(utterance)
    }
}
